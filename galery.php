<?php 
session_start();

if(!isset($_SESSION['online']))
{
  header('Location: index.php');
  exit();
}
?>


<div id="login"><h1>Dynamiczna Galeria</h1></div>
<div id="gallery_box">
<?php 
require_once "php/database/connect.php";
$connect = new mysqli($host, $db_user, $db_password, $db_name);
$id_user=$_SESSION['id'];
$result=$connect->query("SELECT COUNT(*) as con FROM Imgs WHERE id='$id_user' OR status='basic'");
$data=mysqli_fetch_assoc($result);
$count=$data['con'];
if($result_w = $connect->query("SELECT src, alt, status FROM Imgs WHERE id='$id_user' OR status='basic'"))
	{
		$i=0;
		echo '<div class="row">';
		foreach ($result_w as $row)
		{ 
			$i++;
			$src[$i]=$row['src'];
			$alt[$i]=$row['alt'];
		}
		for($l=1;$l<=$i; $l++)
{
		echo'  <div class="column">
    <img src="'.$src[$l].'" alt="'.$alt[$l].'" class="gallery hover-shadow cursor" onclick="openModal();currentSlide('.$l.')">
  </div>';
}
echo '</div>
  <div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">';

for($l=1;$l<=$i; $l++)
{
	echo ' <div class="mySlides">
      <div class="numbertext">'.$l.'/'.$i.'</div>
      <img src="'.$src[$l].'" class="gallery">
    </div>';
    }
    echo ' <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>
    <div class="img_box">';
    for($l=1;$l<=$i; $l++)
    {
    	echo ' <div class="column_small">
    	 <img class="demo cursor gallery" src="'.$src[$l].'" onclick="currentSlide('.$l.')" alt="'.$alt[$l].'">
    </div>';
	}
	echo '
    </div>
 </div>
</div>';
		}
	?>

<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

<?php
	$connect->close();
		?>
  </div>