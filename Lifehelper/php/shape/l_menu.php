<nav class="navbar navbar-dark navbar-expand-md bg-default justify-content-center">
    <a href="ind.php" class="navbar-brand d-flex mr-auto ml-3"><i class="fab fa-hire-a-helper mr-1"></i>Lifehelper</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse" id="collapsingNavbar">
        <ul class="navbar-nav justify-content-left ml-2">
        </ul>
        <ul class="nav navbar-nav ml-auto justify-content-end">
            <li class="nav-item ml-2">
                <a class="nav-link" href="log.php"><i class="fas fa-sign-in-alt mr-1"></i>Logowanie</a>
            </li>
            <li class="nav-item">
                <a class="nav-link ml-2" href="registration.php"><i class="fas fa-user-plus mr-1"></i>Rejestracja</a>
            </li>
            <li>
              <td><span class="glyphicon glyphicon-remove"></span>   <span class="glyphicon glyphicon-edit"></span>
              </td>
        </ul>
    </div>
</nav>