<?php
if(isset($_SESSION['online']))
{
  ?>
  <div id="main_menu">
<nav class="navbar navbar-dark navbar-expand-md bg-default justify-content-center">
    <a href="ind.php" class="tp navbar-brand d-flex mr-auto ml-3"><i class="fab fa-hire-a-helper mr-1"></i>Lifehelper</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse" id="collapsingNavbar">
        <ul class="navbar-nav justify-content-left ml-2">
            <li class="nav-item">
                <a class="tp nav-link" href="galery.php"><i class="fas fa-images mr-1"></i>Galeria</a>
            </li>
            <li class="nav-item">
                <a class="tp nav-link" href="todoist.php"><i class="fas fa-clipboard-list mr-1"></i>Lista zadań</a>
            </li>
            <li class="nav-item">
                <a class="tp nav-link" href="snake.php"><i class="fas fa-gamepad mr-1"></i>Canvas-Snake</a>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto justify-content-end">
            <li class="nav-item ml-2">
                <ul class="navbar-nav mt-2 mt-md-0">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="userMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <i class="fas fa-user mr-1"></i>login/email
                  </a>
                  <div class="dropdown-menu" aria-labelledby="userMenu">
                    <a class="dropdown-item" href="php/registration/logout.php">
                      <i class="fas fa-sign-out-alt mr-1"></i>Wyloguj!</a>
                  </div>
                </li>
              </ul>
            
    </div>
</nav>
</div>
<?php } else {?>
<div id="l_menu">
<nav class="navbar navbar-dark navbar-expand-md bg-default justify-content-center">
    <a href="ind.php" class="tp navbar-brand d-flex mr-auto ml-3"><i class="fab fa-hire-a-helper mr-1"></i>Lifehelper</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse" id="collapsingNavbar">
        <ul class="navbar-nav justify-content-left ml-2">
        </ul>
        <ul class="nav navbar-nav ml-auto justify-content-end">
            <li class="nav-item">
                <a class="nav-link ml-2" href="log.php"><i class="fas fa-sign-in-alt mr-1"></i>Logowanie</a>
            </li>
            <li class="nav-item">
                <a class="nav-link ml-2" href="registration.php"><i class="fas fa-user-plus mr-1"></i>Rejestracja</a>
            </li>
           </ul>
    </div>
</nav>
</div>
<?php } ?>