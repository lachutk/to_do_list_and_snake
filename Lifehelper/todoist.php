<?php 
session_start();

if(!isset($_SESSION['online']))
{
  header('Location: index.php');
  exit();
}
?>
<script>$(document).ready(function() {
    $("#menu").load("main_menu.php");
    </script>
    
	<div id="name_and_ex">
	<?php

	//WYŚWIETLANIE NAZWY, ILOŚCI WYKONANYCH ZADAŃ ORAZ PUNKTÓW I USUWANIE STARYCH ZADAŃ, KTÓRE ZOSTAŁY WYKONANE
		require_once "php/database/connect.php";
		$connect = new mysqli($host, $db_user, $db_password, $db_name);
		$id_user=$_SESSION['id'];
		$today=date('Y-m-d');
		if($result_w = $connect->query("SELECT COUNT(*) as total FROM to_do_list WHERE id='$id_user' AND date_of_end_ex='$today'"))//sprawdza ile zadan dzis wykonal uzytkownik
		{
			$data=mysqli_fetch_assoc($result_w);
			$a=$data['total']%10;//sprawdzamy ostatnią cyfrę aby dobre slowo dobrać
        		echo "<p> Witaj ".$_SESSION['user']."! Dziś wykonałeś ".$data['total'];
        		if($a==1) echo " zadanie<br /><br />";
        		else
        		{
        		if(1<$a && $a<5) echo " zadania<br /><br />";
        		else if($a>4) echo " zadań<br /><br />";
        		}
        		$_SESSION['daily_points']=0;
        		for($i=0; $i<$data['total']; $i++)
        		{
        			if(0<=$i && $i<4) $_SESSION['daily_points']+=10;
        			if(3<$i && $i<8) $_SESSION['daily_points']+=12;
        			if(7<$i && $i<12) $_SESSION['daily_points']+=15;
        			if(11<$i) $_SESSION['daily_points']+=20;
        		}
        		echo " Zdobyłeś ". $_SESSION['daily_points'];
        		$b=$_SESSION['daily_points'];
        		if($b==1) echo " punkt.";
        		if(1<$b && $b<5) echo " punkty.";
        		else echo " punktów.";
        		echo "<br /><br />";
        	}
        	$dat=date('Y-m-d');
        	$connect->query("DELETE FROM to_do_list WHERE repeat_ex='' AND count_or_not>0 AND date_of_end_ex<'$dat'");

        	$connect->close();

        	// DODAWANIE KOLEJNYCH ZADAŃ
        	?>

     </div>
     <div id="addd">
        	<p><button id="eject" class="button_plus">Nowe</button>
	 		<br /></p>
        	<form id="eject_ex">

	 			<input type="text" name="name_ex" placeholder="Zadanie które chcesz wykonać" class="input_name" required /> <br />

				<br />Data: <br />
<!-- przycisk on/off -->
				<label class="switch">

	 				<input onclick="disabledCheckboxes('todisableddate', this);" type="checkbox" name="if_date" checked="checked" />
					<span class="checkmark_if"></span>

				</label>
	 			<br />
				<div id="todisableddate"><input type="date" name="date_ex" value="<?php echo date("Y-m-d"); ?>" /></div><br />

	 			Powtarzalność: <br />
				<label class="switch">

	 				<input onclick="disabledCheckboxes('todisabledrep', this);" type="checkbox" name="if_repeat" />
					<span class="checkmark_if"></span>

				</label>

				<div id="todisabledrep">
					
					<label><input type="hidden" name="everyday"><input onclick="setAllCheckboxes('repeatch', this);" type="checkbox" name="everyday" value="codziennie" disabled="true" />codziennie</label>
					<div id="repeatch">
						<label><input type="hidden" name="monday"><input type="checkbox" class="mr-1" name="monday" value="poniedziałek" disabled="true" />poniedziałek</label>
						<label><input type="hidden" name="tuesday"><input type="checkbox" class="mr-1" name="tuesday" value="wtorek" disabled="true"/>wtorek</label>
						<label><input type="hidden" name="wednesday"><input type="checkbox" class="mr-1" name="wednesday" value="środa" disabled="true"/>środa</label>
						<label><input type="hidden" name="thursday"><input type="checkbox" class="mr-1" name="thursday" value="czwartek" disabled="true"/>czwartek</label>
						<label><input type="hidden" name="friday"><input type="checkbox" class="mr-1" name="friday" value="piątek" disabled="true"/>piątek</label>
						<label><input type="hidden" name="saturday"><input type="checkbox" class="mr-1" name="saturday" value="sobota" disabled="true"/>sobota</label>
						<label><input type="hidden" name="sunday"><input type="checkbox" class="mr-1" name="sunday" value="niedziela" disabled="true"/>niedziela</label><br />
						<input type="button" name="addtodo" value="Dodaj" class="button_1" onclick="addex()" />
					</div>
				</div>
				<?php 
	 				if(isset($_SESSION['e_if_repeat']))
	 					{
	 						echo '<div class="error">'.$_SESSION['e_if_repeat'].'</div>';
	 						unset($_SESSION['e_if_repeat']);
	 					}
	 			?>
        	</form>
        </div>

        	<br /><br /><br />


<div id="table_ex">
<?php

//WYPISYWANIE ZADAŃ
require_once "php/database/connect.php";
$connect = new mysqli($host, $db_user, $db_password, $db_name);
$id_user=$_SESSION['id'];
if($result_w = $connect->query("SELECT Id_ex, name_ex, date_ex, repeat_ex, date_of_end_ex FROM to_do_list WHERE id='$id_user' ORDER BY date_ex"))
	{
	$today="today";
	echo '<span onclick="toggle(\''.$today.'\')"> <h1>Dzisiejsze zadania: </h1><i>Kliknij, aby rozwinąć/zwinąć</i></span><br /><br /><br />
	<div class="table-responsive"> 
	<div id="today">
	 <table class="table" id="tabletoday">';
	$i=0;
	foreach ($result_w as $row) 
		{
			$i++;
			$toggle="toggle_ex_".$i;
			$Id_ex=$row['Id_ex'];
		$date_ex=date_create($row['date_ex']); //tworzona jest zmienna opierająca się o daty
		$date_of_end_ex=date_create($row['date_of_end_ex']);
		$repeat_ex=explode(",", $row['repeat_ex']);//wypisuje ze stringa do tablicy
		$rep_size=count($repeat_ex);
		if(date_format($date_ex, 'Y-m-d') == date('Y-m-d')&& date_format($date_of_end_ex, 'Y-m-d')!=date('Y-m-d')) //zmieniam dluga datę na Rok/miesiąc/dzień i porównuje z dziś po czym sprawdzam czy dziś już nie wykonałem tego zadania
			{
				echo  '<tr><td class="td_checkbox">
				<input type="checkbox" name="ex_end" onclick="deleteAjax('.$Id_ex.')"/>
				</td><td class="td_big">'. $row['name_ex']. '</td>';
				if($row['date_ex']!="0000-00-00 00:00:00")
					{
						echo '<td class="td_mid">Dzisiaj</td>';
					}
					else
						echo '<td class="td_small"></td>';
				echo '<td class="td_small">'.  $row['repeat_ex']. '</td> ';
	include "php/edittargetex.php"; 
				  echo '</p></td></tr>';
			}
			else
			{
				for($l=0; $l<$rep_size; $l++) // sprawdza 7 dni
				{
					if($repeat_ex[$l]=="poniedziałek")$repeat_ex[$l]="Monday";
					if($repeat_ex[$l]=="wtorek")$repeat_ex[$l]="Tuesday";
					if($repeat_ex[$l]=="środa")$repeat_ex[$l]="Wednesday";
					if($repeat_ex[$l]=="czwartek")$repeat_ex[$l]="Thursday";
					if($repeat_ex[$l]=="piątek")$repeat_ex[$l]="Friday";
					if($repeat_ex[$l]=="sobota")$repeat_ex[$l]="Saturday";
					if($repeat_ex[$l]=="niedziela")$repeat_ex[$l]="Sunday";


					if(($repeat_ex[$l]==date("l") && date_format($date_of_end_ex, 'Y-m-d')!=date('Y-m-d')) || ($repeat_ex[$l]=="codziennie" && date_format($date_of_end_ex, 'Y-m-d')!=date('Y-m-d'))) //porównuje z dzisiaj lub czy nie codziennie i czy nie bylo dzis wykonane
					{

						echo  '<tr><td class="td_checkbox"><input type="checkbox" name="ex_end" onclick="deleteAjax('.$Id_ex.')"/></td><td class="td_big">'. $row['name_ex']. '</td>';
						if($row['date_ex']!="0000-00-00 00:00:00")
						echo '<td class="td_mid">'. date_format($date_ex, 'Y-m-d'). '</td>';
						else
							echo '<td class="td_mid"></td>';
				echo '<td class="td_small">'.  $row['repeat_ex']. '</td> ';
				for($k=0; $k<$rep_size; $k++) // sprawdza 7 dni
				{
					if($repeat_ex[$k]=="Monday")$repeat_ex[$k]="poniedziałek";
					if($repeat_ex[$k]=="Tuesday")$repeat_ex[$k]="wtorek";
					if($repeat_ex[$k]=="Wednesday")$repeat_ex[$k]="środa";
					if($repeat_ex[$k]=="Thursday")$repeat_ex[$k]="czwartek";
					if($repeat_ex[$k]=="Friday")$repeat_ex[$k]="piątek";
					if($repeat_ex[$k]=="Saturday")$repeat_ex[$k]="sobota";
					if($repeat_ex[$k]=="Sunday")$repeat_ex[$k]="niedziela";
				}
				include "php/edittargetex.php"; 
				break;
					}
				}

			}


		}
		echo '</table></div></div> <br /><br /><br />';

					$tomorrow="tomorrow";
					echo '<span onclick="toggle(\''.$tomorrow.'\')"> <h1>Jutro do zrobienia: </h1><i>Kliknij, aby rozwinąć/zwinąć</i></span><br /><br /><br />
					<div class="table-responsive">
					<div class="toggle_content" id="tomorrow">
					<table class="table">';
					foreach ($result_w as $row) 
		{
			$i++;
			$toggle="toggle_ex_".$i;
			$Id_ex=$row['Id_ex'];
		$date_ex=date_create($row['date_ex']); //tworzona jest zmienna opierająca się o daty
		$date_of_end_ex=date_create($row['date_of_end_ex']);
		$repeat_ex=explode(",", $row['repeat_ex']);//wypisuje ze stringa do tablicy
		$rep_size=count($repeat_ex);
		if(date_format($date_ex, 'Y-m-d') == date('Y-m-d', strtotime("+1 day")) && date_format($date_of_end_ex, 'Y-m-d')!=date('Y-m-d')) //zmieniam dluga datę na Rok/miesiąc/dzień i porównuje z nastepnym dniem 
			{
				echo  '<tr><td class="td_checkbox"><input type="checkbox" name="ex_end" onclick="deleteAjax('.$Id_ex.')"/></td><td class="td_big">'. $row['name_ex']. '</td>';
				if($row['date_ex']!="0000-00-00 00:00:00.000000")
					{
						echo '<td class="td_mid">Jutro</td>';
					}
					else
						echo '<td class="td_mid"></td>';
				echo '<td class="td_small">'.  $row['repeat_ex']. '</td> ';
				include "php/edittargetex.php"; 
			}
			else
				{
				for($l=0; $l<$rep_size; $l++) // sprawdza 7 dni
				{
					if($repeat_ex[$l]=="poniedziałek")$repeat_ex[$l]="Monday";
					if($repeat_ex[$l]=="wtorek")$repeat_ex[$l]="Tuesday";
					if($repeat_ex[$l]=="środa")$repeat_ex[$l]="Wednesday";
					if($repeat_ex[$l]=="czwartek")$repeat_ex[$l]="Thursday";
					if($repeat_ex[$l]=="piątek")$repeat_ex[$l]="Friday";
					if($repeat_ex[$l]=="sobota")$repeat_ex[$l]="Saturday";
					if($repeat_ex[$l]=="niedziela")$repeat_ex[$l]="Sunday";

					if($repeat_ex[$l]==date("l", strtotime("+1 day")) | $repeat_ex[$l]=="codziennie") //porównuje z dzisiaj lub czy nie codziennie
					{
						echo  '<tr><td class="td_checkbox"></td><td class="td_big">'. $row['name_ex']. '</td>';
						if($row['date_ex']!="0000-00-00 00:00:00")
						echo '<td class="td_mid">'. date_format($date_ex, 'Y-m-d'). '</td>';
						else
							echo '<td class="td_mid"></td>';
				echo '<td class="td_small">'.  $row['repeat_ex']. '</td> ';
				for($k=0; $k<$rep_size; $k++) // sprawdza 7 dni
				{
					if($repeat_ex[$k]=="Monday")$repeat_ex[$k]="poniedziałek";
					if($repeat_ex[$k]=="Tuesday")$repeat_ex[$k]="wtorek";
					if($repeat_ex[$k]=="Wednesday")$repeat_ex[$k]="środa";
					if($repeat_ex[$k]=="Thursday")$repeat_ex[$k]="czwartek";
					if($repeat_ex[$k]=="Friday")$repeat_ex[$k]="piątek";
					if($repeat_ex[$k]=="Saturday")$repeat_ex[$k]="sobota";
					if($repeat_ex[$k]=="Sunday")$repeat_ex[$k]="niedziela";
				}
				include "php/edittargetex.php"; 
				break;
					}
				}

			}
		}
		echo '</table></div></div><br /><br /><br />';

	$all="all";
	echo '<span onclick="toggle(\''.$all.'\')"><h1> Wszystkie zadania: </h1><i>Kliknij, aby rozwinąć/zwinąć</i></span><br /><br /><br />
	<div class="table-responsive">
	<div class="toggle_content" id="all">
					<table class="table">';
					foreach ($result_w as $row) 
		{
			$i++;
			
			$toggle="toggle_ex_".$i;
			$Id_ex=$row['Id_ex'];
		$date_ex=date_create($row['date_ex']); //tworzona jest zmienna opierająca się o daty
		$date_of_end_ex=date_create($row['date_of_end_ex']);
		$repeat_ex=explode(",", $row['repeat_ex']);
		$rep_size=count($repeat_ex);
				if($row['date_ex']!="0000-00-00 00:00:00.000000" &&  date_format($date_of_end_ex, 'Y-m-d')!=date('Y-m-d') && $row['repeat_ex']=="")
					{

						echo  '<tr><td class="td_checkbox"><input type="checkbox" name="ex_end" onclick="deleteAjax('.$Id_ex.')"/></td><td class="td_big">'. $row['name_ex']. '</td>';
						echo '<td class="td_mid">'. date_format($date_ex, 'Y-m-d'). '</td>';
						echo '<td class="td_small"></td> ';
						include "php/edittargetex.php"; 
					}
					if($row['repeat_ex']!="")
					{
						echo  '<tr><td class="td_checkbox"></td><td class="td_big">'. $row['name_ex']. '</td>';
						if($row['date_ex']!="0000-00-00 00:00:00")
						echo '<td class="td_mid">'. date_format($date_ex, 'Y-m-d'). '</td>';
						else
							echo '<td class="td_mid"></td>';
				echo '<td class="td_small">'.  $row['repeat_ex']. '</td> ';
				include "php/edittargetex.php"; 
					}
		}
		echo '</table></div></div> <br /><br /><br />';

	}

$connect->close();
?>
</div>


 <script type="text/javascript" src="js/disabledcheckboxes.js"></script>
	<script type="text/javascript" src="js/allchecked.js"> </script>	
	<script type="text/javascript" src="jquery/eject.js"> </script>	
	<script type="text/javascript" src="jquery/jquery.js"> </script>	
	<script type="text/javascript" src="ajax/deleteclick.js"> </script>	
	<script type="text/javascript" src="ajax/deleteit.js"> </script>	
	<script type="text/javascript" src="ajax/editit.js"> </script>	
	<script type="text/javascript" src="ajax/ajax.js"> </script>



